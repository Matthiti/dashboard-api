# Dashboard API

## Requirements

- `node`
- `npm`
- Valid kubernetes config at `$HOME/.kube/config`

## Install

```bash
npm i
```

## Run

```bash
npm run dev
```