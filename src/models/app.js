const { DataTypes } = require('sequelize');

const db = require('../db');

const App = db.define('App', {
  name: {
    type: DataTypes.STRING,
    allowNull: false
  },
  url: {
    type: DataTypes.STRING,
    allowNull: false
  }
}, { timestamps: false });

module.exports = App;