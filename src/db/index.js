const Sequelize = require('sequelize');

const { DB_HOST, DB_PORT, DB_USER, DB_PASSWORD, DB_NAME } = require('../config');

const db = new Sequelize(DB_NAME, DB_USER, DB_PASSWORD, {
  host: DB_HOST,
  port: DB_PORT,
  dialect: 'mysql'
});

db.authenticate();

module.exports = db;