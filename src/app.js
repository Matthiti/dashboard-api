const express = require('express');
const morgan = require('morgan');
const helmet = require('helmet');

require('dotenv').config();

const api = require('./api');

const app = express();

app.use(morgan('dev'));
app.use(helmet());
app.use(express.json());

app.get('/', (req, res) => {
  res.json({
    message: 'Working 😎'
  });
});

app.use('/api/v1', api);

module.exports = app;