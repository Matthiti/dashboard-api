const express = require('express');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const User = require('../models/user');
const { JWT_SECRET } = require('../config');

const router = express.Router();

router.post('/login', async (req, res) => {
  if (!req.body || !req.body.email || !req.body.password) {
    res.status(400);
    res.json({
      message: 'Missing email or password'
    });
    return;
  }

  const { email, password } = req.body;
  const user = await User.findOne({ where: { email } });
  if (!user) {
    res.status(401);
    res.json({
      message: 'Invalid email or password'
    });
    return;
  }

  const passwordsMatch = await bcrypt.compare(password, user.password);
  if (!passwordsMatch) {
    res.status(401);
    res.json({
      message: 'Invalid email or password'
    });
    return;
  }

  const token = jwt.sign({
    subject: user.id,
    issuer: 'dashboard',
    iat: Date.now() / 1000,
    exp: Date.now() / 1000 + 3600
  }, JWT_SECRET);

  res.json({ token });
});

module.exports = router;