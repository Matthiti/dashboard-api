const express = require('express');

const auth = require('./auth');
const me = require('./me');
const users = require('./users');
const kubernetes = require('./kubernetes');
const apps = require('./apps');
const jwtMiddleware = require('../middleware/jwt');

const router = express.Router();
router.use('/auth', auth);
router.use(jwtMiddleware);
router.use('/me', me);
router.use('/users', users);
router.use('/kubernetes', kubernetes);
router.use('/apps', apps);

module.exports = router;