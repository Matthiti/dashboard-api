const express = require('express');

const router = express.Router();

router.get('/', async (req, res) => {
  res.json(res.locals.user);
});

module.exports = router;