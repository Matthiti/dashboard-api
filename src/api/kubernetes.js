const express = require('express');
const k8s = require('@kubernetes/client-node');

const kc = new k8s.KubeConfig();
kc.loadFromDefault();

const k8sApi = kc.makeApiClient(k8s.CoreV1Api);

const router = express.Router();

router.get('/services', async (req, res) => {
  try {
    const services = await k8sApi.listServiceForAllNamespaces().then(res => res.body.items);
    res.json(services);
  } catch (err) {
    res.status(503);
    res.json({
      message: 'Cannot connect to Kubernetes API'
    });
  }
});

router.get('/pods', async (req, res) => {
  try {
    const pods = await k8sApi.listPodForAllNamespaces().then(res => res.body.items);
    res.json(pods);
  } catch (err) {
    res.status(503);
    res.json({
      message: 'Cannot connect to Kubernetes API'
    });
  }
});

router.get('/nodes', async (req, res) => {
  try {
    const pods = await k8sApi.listNode().then(res => res.body.items);
    res.json(pods);
  } catch (err) {
    res.status(503);
    res.json({
      message: 'Cannot connect to Kubernetes API'
    });
  }
})

module.exports = router;