const express = require('express');

const App = require('../models/app');

const router = express.Router();

router.get('/', async (req, res) => {
  const apps = await App.findAll();
  res.json(apps);
});

router.get('/:id', async (req, res) => {
  const { id } = req.params;
  const app = await App.findByPk(id);
  if (app) {
    res.json(app);
  } else  {
    res.status(404);
    res.json({
      message: `Unknown app with id ${id}`
    });
  }
});

router.post('/', async (req, res) => {
  try {
    const app = await App.create(req.body);
    res.status(201);
    res.json(app);
  } catch (err) {
    res.status(400);
    res.json({
      message: 'Invalid app'
    });
  }
});

router.put('/:id', async (req, res) => {
  const { id } = req.params;
  const app = await App.findByPk(id);
  if (!app) {
    res.status(404);
    res.json({
      message: `Unknown app with id ${id}`
    });
    return;
  }

  app.set(req.body);
  await app.save();
  res.json(app);
});

router.delete('/:id', async (req, res) => {
  const { id } = req.params;
  const app = await App.findByPk(id);
  if (!app) {
    res.status(404);
    res.json({
      message: `Unknown app with id ${id}`
    });
    return;
  }

  await app.destroy();
  res.status(204).send();
});

module.exports = router;