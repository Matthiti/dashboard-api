const express = require('express');

const User = require('../models/user');

const router = express.Router();

router.get('/', async (req, res) => {
  const users = await User.findAll({
    attributes: {
      exclude: ['password']
    }
  });
  res.json(users);
});

router.post('/', async (req, res) => {
  try {
    const user = await User.create(req.body);
    delete user.dataValues.password;
    res.status(201);
    res.json(user);
  } catch (err) {
    if (err.name === 'SequelizeUniqueConstraintError') {
      res.status(409);
      res.json({
        message: 'Email already in use'
      });
    } else {
      res.status(400);
      res.json({
        message: 'Invalid user'
      });
    }
  }
});

module.exports = router;