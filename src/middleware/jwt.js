const jwt = require('jsonwebtoken');

const { JWT_SECRET } = require('../config');
const User = require('../models/user');

const jwtMiddleware = async (req, res, next) => {
  const authHeader = req.header('X-Authorization');
  if (!authHeader) {
    res.status(401);
    res.json({
      message: "Missing 'X-Authorization' header"
    });
    return;
  }

  if (!authHeader.startsWith('Bearer ')) {
    res.status(401);
    res.json({
      message: "Invalid 'X-Authorization' header"
    });
    return;
  }

  const token = authHeader.slice('Bearer '.length);
  try {
    const decoded = jwt.verify(token, JWT_SECRET);
    const user = await User.findByPk(decoded.subject);
    delete user.dataValues.password;
    res.locals.user = user;
    next();
  } catch (_) {
    res.status(401);
    res.json({
      message: 'Invalid token'
    });
  }
};

module.exports = jwtMiddleware;